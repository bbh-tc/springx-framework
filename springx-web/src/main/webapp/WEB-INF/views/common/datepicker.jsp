<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<link rel="stylesheet" href="${ctx}/resources/ace/dist/css/datepicker.min.css"/>
<script src="${ctx}/resources/ace/dist/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="${ctx}/resources/js/plugins/date-time/datepicker.extend.js"></script>