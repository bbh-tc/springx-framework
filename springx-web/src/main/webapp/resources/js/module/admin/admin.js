//请指定操作的表格Id,以免多次指定
dataTableJs.tableId = 'dynamic-table';
var oTable1;
$(function () {

    $('.date-picker').datepicker().next().on(ace.click_event, function(){
        $(this).prev().focus();
    });

    oTable1 =
        $('#' + dataTableJs.tableId).dataTable($.extend(dataTableJs.default, {
            "ajax": {
                url: ctx + "/admin/list", data: function (d) {
                    delete d.columns;
                    //增加page和pageSize
                    if (oTable1) {
                        d._page = oTable1.api().page();
                        d._pageSize = oTable1.api().page.len();
                    }
                    //增加查询
                    return dataTableJs.serialize(d,"#search-form");
}
            },
            "order": [[4, "desc"]], /**默认排序*/
            "columns": [
                {"data": null, orderable: false},
                {"data": "username"},
                {"data": "name"},
                {"data": "email"},
                {"data": "createDate"},
                {"data": "isEnabled"},
                {"data": "isLocked"},
                {"data": null, orderable: false}
            ],
            columnDefs: [
                {
                    targets: 0,
                    render: function (a, b, c, d) {
                        return '<div class="center"><label class="pos-rel"><input type="checkbox" class="ace" value="' + c.id + '"> <span class="lbl"></span></label></div>';
                    }
                },
                {
                    targets:5,
                    render: function (a, b, c, d) {
                        return c.isEnabled?"启用":"禁用";
                    }
                },
                {
                    targets:6,
                    render: function (a, b, c, d) {
                        return c.isLocked?"锁定":"正常";
                    }
                },
                {
                    targets:7,
                    render: function (a, b, c, d) {
                        return dataTableJs.buttonGroupTemplate([
                            {
                                "name": "编辑",
                                "fn": "doEdit(" + c.id + ")",
                                "type": "purple"
                            },
                            {"name": "密码初始化", "fn": "doInit(" + c.id + ")", "type": "pink"}
                        ]);
                    }
                }

            ]
        }));

})

function formCheck() {
    return $('#editForm').validate({
        rules: {
            username: {
                required: true,
                startLetters: true,
                wRangeLength: [6, 12]
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                wRangeLength: [6, 12]
            },
            repassword: {
                required: true,
                equalTo: "#password"
            },
            url: {
                required: true,
                url: true
            },
            comment: {
                required: true
            },
            state: {
                required: true
            }
        }
    });
}

function buildButton(type) {
    var btnTemplate = {
        className: "btn2"==type?"btn-default":"btn-info"
    };
    var labelObject={"btn0":"保存","btn1":"保存继续","btn2":"关闭","btn3":"保存"};
    btnTemplate.label = labelObject[type];
    btnTemplate.callback = function () {
        if ("btn2" == type) {
            return true;//关闭
        }
        if (!formCheck().form()) {
            return false;
        }
        var $form = $("#editForm");
        if("btn0"==type||"btn1"==type){
            $.getJSON($form.prop("action"), $form.serialize(), function (data) {
                $.messager.alert0({selector: "#editAlert", type: data.status, html: data.body});
                if (data && data.status == "SUCCESS") {
                    if ("btn0" == type) {
                        oTable1.api().ajax.reload();
                        $.bootbox.hideAll();
                    }
                }
            });
        }
        else if("btn3"==type){
            $.getJSON(ctx+"/admin/update", $form.serialize(), function (data) {
                $.messager.alert0({selector: "#editAlert", type: data.status, html: data.body});
                if (data && data.status == "SUCCESS") {
                    oTable1.api().ajax.reload();
                    $.bootbox.hideAll();
                }
            });
        }
        return false;
    }
    return btnTemplate;

}

function  doEdit(id){
    var url = ctx + "/admin/edit_none?id="+id;
    var title = "用户编辑";
    var buttons={
        btn0: buildButton("btn3"),//编辑时候的保存按钮
        btn2: buildButton("btn2")
    };
    $.messager.dialog({url: url, title: title, buttons: buttons});
}

function doAdd() {
    var url = ctx + "/admin/edit_none";
    var title = "用户增加";
    var buttons = {
        btn0: buildButton("btn0"),
        btn1: buildButton("btn1"),
        btn2: buildButton("btn2")
    };
    $.messager.dialog({url: url, title: title, buttons: buttons});
}
/**
 *删除
 */
function doDelete() {
    var cks = dataTableJs.getChecks();
    if (cks.size() == 0) {
        $.messager.alert("请选择需要删除的数据项");
        return;
    }
    var url = ctx + "/admin/delete";
    $.messager.confirm("你确定要删除选中的记录吗?", function (ck) {
        if (ck) {
            var ary = [];
            $.each(cks, function (i) {
                ary.push(this.value);
            });
            $.getJSON(url, {ids: ary}, function (data) {
                $.messager.alert0({selector: "#indexAlert", type: data.status, html: data.body});
                if (data && data.status == 'SUCCESS') {
                    oTable1.api().ajax.reload();
                }
            });
        }
    });
}
/**查询*/
function doSearch(){
    oTable1.api().ajax.data={"params": $("#search-form").serialize()};
    oTable1.api().ajax.reload();
}