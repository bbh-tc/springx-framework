package com.springx.shiro.service;

import com.springx.shiro.domain.Account;
import com.springx.shiro.domain.ShiroResource;
import com.springx.shiro.domain.ShiroRole;

import java.util.List;
import java.util.Map;

/**
 * Created by test on 2015/8/26.
 */
public interface ShiroAccountService {
    public static final String HASH_ALGORITHM = "SHA-1";
    public static final int HASH_INTERATIONS = 1024;

    /**
     * 查询所有角色
     *
     * @return
     */
    List<ShiroRole> findAllRole();

    /**
     * 通过登录名查询角色
     *
     * @param loginName
     * @return
     */
    List<ShiroRole> findRoleListByLoginName(String loginName);

    /**
     * 通过角色查询资源权限
     *
     * @param roleId
     * @return
     */
    List<String> findPermissionListByRole(Long roleId);

    /**
     * 查询所有资源权限
     *
     * @return
     */
    List<String> findAllPermissionList();

    /**
     * 查询所有资源
     *
     * @return
     */
    List<ShiroResource> findAllResource();

    /**
     * 通过登录名查询所有菜单资源
     *
     * @param loginName
     * @return
     */
    Map<Long, List<ShiroResource>> findMenuListByLoginName(String loginName);


    /**
     * 通过登录名查询账号
     *
     * @param loginName
     * @return
     */
    Account findAccountByLoginName(String loginName);

    /**
     * 判断用户是否拥有最高权限
     *
     * @param loginName
     * @return
     */
    Boolean isSuperAdmin(String loginName);
}
