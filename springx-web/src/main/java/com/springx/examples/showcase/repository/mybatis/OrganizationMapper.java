package com.springx.examples.showcase.repository.mybatis;

import com.springx.examples.showcase.entity.Organization;
import com.springx.examples.showcase.vo.tree.ZTreeOrganizationVo;
import com.springx.examples.showcase.vo.tree.ZTreeResourceVo;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by roman_000 on 2015/8/9.
 */
@MyBatisRepository
public interface OrganizationMapper extends Mapper<Organization>{

    /**
     * 查询下级记录
     * @param params
     * @return
     */
    List<ZTreeOrganizationVo> findChildren(Map<String,Object> params);
}
