package com.springx.examples.showcase.controller;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.springx.examples.showcase.common.domain.Message;
import com.springx.examples.showcase.common.domain.Order;
import com.springx.examples.showcase.vo.tree.ZTreeNoneAjaxVo;
import com.springx.examples.showcase.vo.tree.ZTreeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.springx.examples.showcase.entity.Resource;
import com.springx.examples.showcase.service.ResourceService;
import com.springx.examples.showcase.vo.tree.ZTreeRootVo;
import com.springx.examples.showcase.vo.tree.ZTreeResourceVo;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * Created by roman_000 on 2015/7/12.
 */
@Controller
@RequestMapping("/resource")
public class ResourceController extends BaseController {
    @Autowired
    private ResourceService resourceService;

    @RequestMapping("index")
    public String index(Long id) {
        return "/resource/index";
    }

    @RequestMapping("/list")
    public
    @ResponseBody
    List list(Long id) {
        ZTreeRootVo rootVo;
        Map<String, Object> params = Maps.newHashMap();
        params.put("parentId", null==id?-1:id);
        params.put(Order.PROPERTY_NAME, new Order("sort", Order.ASC));
        List<ZTreeResourceVo> list = resourceService.findChildren(params);
        if (null == id) {
            rootVo = ZTreeRootVo.initRoot("资源列表");
            rootVo.setChildren(list);
            List<ZTreeVo> rootTreeList = Lists.newArrayList();
            rootTreeList.add(rootVo);
            return rootTreeList;
        }
        return list;
    }

    /**
     * 保存资源
     *
     * @param resource
     * @return
     */
    @RequestMapping("/insert")
    public
    @ResponseBody
    Message insert(@Valid Resource resource) {
        resourceService.save(resource);
        Message message = new Message(Message.StatusType.SUCCESS, "保存成功");
        message.setData(resource.getId());
        return message;
    }


    /**
     * 保存资源
     *
     * @param resource
     * @return
     */
    @RequestMapping("/update")
    public
    @ResponseBody
    Message update(@Valid Resource resource) {
        Assert.notNull(resource.getId());
        Resource editResource = resourceService.select(resource.getId());
        editResource.setName(resource.getName());
        editResource.setPermissionKey(null==resource.getPermissionKey()?"":resource.getPermissionKey());
        editResource.setPermissionValue(null==resource.getPermissionValue()?"":resource.getPermissionValue());
        editResource.setUrl(null==resource.getUrl()?"":resource.getUrl());
        editResource.setType(resource.getType());
        if(editResource.getSort()==null){
            editResource.setSort(editResource.getId());
        }
        resourceService.updateByPrimaryKeySelective(editResource);
        Message message = new Message(Message.StatusType.SUCCESS, "保存成功");
        return message;
    }

    /**
     * 删除资源
     *
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    public
    @ResponseBody
    Message delete(Long id) {
        Message message = null;
        Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        int count = resourceService.findChildrenCount(id);
        if (count > 0) {
            message = new Message(Message.StatusType.ERROR, "请先删除子节点");
        } else {
            resourceService.deleteByPrimaryKey(id);
            message = new Message(Message.StatusType.SUCCESS, "删除成功");
        }
        return message;
    }

    @RequestMapping("/sort")
    public
    @ResponseBody
    Message sort(Long id1, Long id2) {
        Assert.notNull(id1);
        Assert.notNull(id2);
        resourceService.sort(id1, id2);
        Message message = new Message(Message.StatusType.SUCCESS, "移动成功");
        return message;
    }

    /**
     * 角色授权
     *
     * @return
     * @throws org.springframework.web.bind.ServletRequestBindingException
     */
    @RequestMapping("/authorization")
    public
    @ResponseBody
    List authorization(Long id) throws ServletRequestBindingException {
        ZTreeRootVo rootVo;
        Map<String, Object> params = Maps.newHashMap();
        params.put("parentId", null==id?-1l:id);
        List<ZTreeNoneAjaxVo> list = resourceService.selectTreeResourceChildren(params);
        if (null == id) {
            rootVo = ZTreeRootVo.initRoot("资源列表");
            rootVo.setChecked(true);
            rootVo.setChildren(list);
            List<ZTreeVo> rootTreeList = Lists.newArrayList();
            rootTreeList.add(rootVo);
            return rootTreeList;
        }
        return list;
    }
}
