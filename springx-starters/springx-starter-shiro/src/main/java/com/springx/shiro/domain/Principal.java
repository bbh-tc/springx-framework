package com.springx.shiro.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 身份信息
 *
 * @author BBH Team
 * @version 1.0
 */
public class Principal implements Serializable {

    private static final long serialVersionUID = 5798882004228239559L;
    /**
     * 用户名
     */
    private String username;

    /**
     * 微信openId
     */
    private String openId;
    /**
     * 微信unionId
     */
    private String unionId;


    /**
     * 用户的菜单集合
     */
    private Map<Long, List<ShiroResource>> menus;


    /**
     * @param username 用户名
     */
    public Principal(String username) {
        this.username = username;
    }

    public Principal(String username, String openId, String unionId) {
        this.username = username;
        this.openId = openId;
        this.unionId = unionId;
    }

    /**
     * 获取用户名
     *
     * @return 用户名
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置用户名
     *
     * @param username 用户名
     */
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return username;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public Map<Long, List<ShiroResource>> getMenus() {
        return menus;
    }

    public void setMenus(Map<Long, List<ShiroResource>> menus) {
        this.menus = menus;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Principal other = (Principal) obj;
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        return true;
    }

}