package com.springx.examples.showcase.vo.tree;

import java.util.List;

/**
 * Created by roman_000 on 2015/7/12.
 */
public class ZTreeNoneAjaxVo extends ZTreeVo {
    private ZTreeNoneAjaxVo parent;
    private List<ZTreeNoneAjaxVo> children;

    public ZTreeNoneAjaxVo getParent() {
        return parent;
    }

    public void setParent(ZTreeNoneAjaxVo parent) {
        this.parent = parent;
    }

    public Object getChildren() {
        return children;
    }

    public void setChildren(List<ZTreeNoneAjaxVo> children) {
        this.children = children;
    }
}
