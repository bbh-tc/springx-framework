package com.springx.examples.showcase.entity;

import javax.persistence.*;

@Table(name = "tb_resource")
public class Resource {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    /**
     * 1.菜单 2.动作
     */
    private String type;

    /**
     * 资源入口
     */
    private String url;

    /**
     * 层级
     */
    private Integer level;

    private Long sort;

    @Column(name = "parent_id")
    private Long parentId;
    /**
     * 权限认证key
     */
    @Column(name = "permission_type")
    private String permissionType;
    /**
     * 权限认证key
     */
    @Column(name = "permission_key")
    private String permissionKey;

    /**
     * 权限认证value
     */
    @Column(name = "permission_value")
    private String permissionValue;

    @Column(name = "is_enabled")
    private Boolean isEnabled;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 获取资源入口
     *
     * @return url - 资源入口
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置资源入口
     *
     * @param url 资源入口
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 获取层级
     *
     * @return level - 层级
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * 设置层级
     *
     * @param level 层级
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    /**
     * @return parent_id
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * @param parentId
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(String permissionType) {
        this.permissionType = permissionType;
    }

    /**
     * 获取权限认证key
     *
     * @return permission_key - 权限认证key
     */
    public String getPermissionKey() {
        return permissionKey;
    }

    /**
     * 设置权限认证key
     *
     * @param permissionKey 权限认证key
     */
    public void setPermissionKey(String permissionKey) {
        this.permissionKey = permissionKey;
    }

    /**
     * 获取权限认证value
     *
     * @return permission_value - 权限认证value
     */
    public String getPermissionValue() {
        return permissionValue;
    }

    /**
     * 设置权限认证value
     *
     * @param permissionValue 权限认证value
     */
    public void setPermissionValue(String permissionValue) {
        this.permissionValue = permissionValue;
    }

    /**
     * @return is_enabled
     */
    public Boolean getIsEnabled() {
        return isEnabled;
    }

    /**
     * @param isEnabled
     */
    public void setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
}