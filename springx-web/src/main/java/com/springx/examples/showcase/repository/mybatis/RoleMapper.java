package com.springx.examples.showcase.repository.mybatis;

import com.springx.examples.showcase.entity.Admin;
import com.springx.examples.showcase.entity.Resource;
import com.springx.examples.showcase.entity.Role;
import com.springx.shiro.domain.ShiroRole;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by roman_000 on 2015/7/16.
 */
@MyBatisRepository
public interface RoleMapper extends Mapper<Role>{

       List<String> findPermissionListByRole(Map<String,Object> params);
       /**
        * 查找所有资源的权限数据
        * @param params
        * @return
        */
       List<String> findAllPermissionList(Map<String,Object> params);
       List<ShiroRole> findRoleListByLoginName(Map<String,Object> params);
}
