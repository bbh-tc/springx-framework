<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<link rel="stylesheet" href="${ctx}/resources/js/plugins/zTree_v3/css/zTreeStyle/zTreeStyle.css"/>
<script type="text/javascript" src="${ctx}/resources/js/plugins/zTree_v3/js/jquery.ztree.all-3.5.min.js"></script>