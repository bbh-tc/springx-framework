package com.springx.shiro.domain;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShiroResource {

    private Long id;

    private String name;
    /**
     * 1.菜单 2.动作
     */
    private String type;

    /**
     * 资源入口
     */
    private String url;

    /**
     * 层级
     */
    private Integer level;

    private Long sort;

    private Long parentId;
    /**
     * 权限认证key
     */
    private String permissionType;

    /**
     * 权限认证key
     */
    private String permissionKey;

    /**
     * 权限认证value
     */
    private String permissionValue;

    private Boolean isEnabled;
    /**
     * 下级菜单集合
     */
    private List<ShiroResource> children;
    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 获取资源入口
     *
     * @return url - 资源入口
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置资源入口
     *
     * @param url 资源入口
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 获取层级
     *
     * @return level - 层级
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * 设置层级
     *
     * @param level 层级
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    /**
     * @return parent_id
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * @param parentId
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(String permissionType) {
        this.permissionType = permissionType;
    }

    /**
     * 获取权限认证key
     *
     * @return permission_key - 权限认证key
     */
    public String getPermissionKey() {
        return permissionKey;
    }

    /**
     * 设置权限认证key
     *
     * @param permissionKey 权限认证key
     */
    public void setPermissionKey(String permissionKey) {
        this.permissionKey = permissionKey;
    }

    /**
     * 获取权限认证value
     *
     * @return permission_value - 权限认证value
     */
    public String getPermissionValue() {
        return permissionValue;
    }

    /**
     * 设置权限认证value
     *
     * @param permissionValue 权限认证value
     */
    public void setPermissionValue(String permissionValue) {
        this.permissionValue = permissionValue;
    }

    /**
     * @return is_enabled
     */
    public Boolean getIsEnabled() {
        return isEnabled;
    }

    /**
     * @param isEnabled
     */
    public void setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public List<ShiroResource> getChildren() {
        return children;
    }

    public void setChildren(List<ShiroResource> children) {
        this.children = children;
    }

    public static void main(String[] args) {
        String testStr = "perms[role:*]";
        Pattern p = Pattern.compile("perms\\[(.*)\\]");
        Matcher m = p.matcher(testStr);
        while(m.find()){
            System.out.println(m.group(1));
        }
    }
}