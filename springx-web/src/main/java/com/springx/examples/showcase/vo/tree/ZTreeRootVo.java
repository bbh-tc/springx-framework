package com.springx.examples.showcase.vo.tree;

import java.util.List;

/**
 * Created by roman_000 on 2015/7/12.
 */
public class ZTreeRootVo extends ZTreeVo {

    private Object children;

    public static ZTreeRootVo initRoot(String name){
        ZTreeRootVo zTreeVo=new ZTreeRootVo();
        zTreeVo.setId(-1l);
        zTreeVo.setName(name);
        zTreeVo.setOpen(true);
        return  zTreeVo;
    }

    public Object getChildren() {
        return children;
    }

    public void setChildren(Object children) {
        this.children = children;
    }
}
