package com.springx.examples.showcase.service;

import com.springx.examples.showcase.entity.Dictionary;
import org.javasimon.aop.Monitored;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
@Transactional
@Monitored

public class DictionaryService extends  BaseService<Dictionary>{
    
}
