package com.springx.examples.showcase.vo;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.List;

/**
 * Created by roman_000 on 2015/7/16.
 */
public class DataTableVo<T> implements Serializable {
    private static final String DRAW_FILED="draw";
    private Integer draw;
    private Long recordsTotal;
    private Long recordsFiltered;
    private List<T> data;
    public DataTableVo(HttpServletRequest request, Page<T> page) throws ServletRequestBindingException {
        this.draw =  ServletRequestUtils.getIntParameter(request,DRAW_FILED);
        this.recordsTotal = page.getTotalElements();
        this.recordsFiltered = page.getTotalElements();
        this.data = page.getContent();
    }

    public Integer getDraw() {
        return draw;
    }

    public void setDraw(Integer draw) {
        this.draw = draw;
    }

    public Long getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(Long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public Long getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(Long recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
