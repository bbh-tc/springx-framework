<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ page import="org.apache.shiro.authc.LockedAccountException "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html class="login-bg">
<head>
    <title>登录</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- bootstrap -->
    <link href="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/css/bootstrap/bootstrap-responsive.css" rel="stylesheet" />
    <link href="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/css/bootstrap/bootstrap-overrides.css" type="text/css" rel="stylesheet" />

    <!-- global styles -->
    <link rel="stylesheet" type="text/css" href="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/css/layout.css" />
    <link rel="stylesheet" type="text/css" href="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/css/elements.css" />
    <link rel="stylesheet" type="text/css" href="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/css/icons.css" />

    <!-- libraries -->
    <link rel="stylesheet" type="text/css" href="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/css/lib/font-awesome.css" />

    <!-- this page specific styles -->
    <link rel="stylesheet" href="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/css/compiled/signin.css" type="text/css" media="screen" />

    <!-- open sans font -->
    <link href='http://fonts.useso.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>


<!-- background switcher -->
<div class="bg-switch visible-desktop">
    <div class="bgs">
        <a href="#" data-img="landscape.jpg" class="bg active">
            <img src="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/img/bgs/landscape.jpg" />
        </a>
        <a href="#" data-img="blueish.jpg" class="bg">
            <img src="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/img/bgs/blueish.jpg" />
        </a>
        <a href="#" data-img="7.jpg" class="bg">
            <img src="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/img/bgs/7.jpg" />
        </a>
        <a href="#" data-img="8.jpg" class="bg">
            <img src="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/img/bgs/8.jpg" />
        </a>
        <a href="#" data-img="9.jpg" class="bg">
            <img src="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/img/bgs/9.jpg" />
        </a>
        <a href="#" data-img="10.jpg" class="bg">
            <img src="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/img/bgs/10.jpg" />
        </a>
        <a href="#" data-img="11.jpg" class="bg">
            <img src="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/img/bgs/11.jpg" />
        </a>
    </div>
</div>


<div class="row-fluid login-wrapper">
    <a href="index.html">
        <img class="logo" src="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/img/logo-white.png" />
    </a>

    <div class="span4 box">
        <div class="content-wrap">
            <h6>登<span style="padding-right: 15px"></span>录</h6>
            <input class="span12" type="text" placeholder="请输入用户名" />
            <input class="span12" type="password" placeholder="请输入秘密" />
            <a href="#" class="forgot">忘记密码?</a>
            <div class="remember">
                <input id="remember-me" type="checkbox" />
                <label for="remember-me">记住我</label>
            </div>
            <a class="btn-glow primary login" href="${ctx}">登 录</a>
        </div>
    </div>
</div>

<!-- scripts -->
<script src="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/js/jquery-latest.js"></script>
<script src="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/js/bootstrap.min.js"></script>
<script src="http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/js/theme.js"></script>

<!-- pre load bg imgs -->
<script type="text/javascript">
    $(function () {
        // bg switcher
        var $btns = $(".bg-switch .bg");
        $btns.click(function (e) {
            e.preventDefault();
            $btns.removeClass("active");
            $(this).addClass("active");
            var bg = $(this).data("img");

            $("html").css("background-image", "url('http://www.17sucai.com/preview/2/2015-03-27/%E7%AE%80%E6%B4%81%E5%90%8E%E5%8F%B0%E6%A8%A1%E6%9D%BF/img/bgs/" + bg + "')");
        });

    });
</script>

</body>
</html>