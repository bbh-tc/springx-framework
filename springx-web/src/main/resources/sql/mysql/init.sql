/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50041
Source Host           : localhost:3306
Source Database       : showcase4

Target Server Type    : MYSQL
Target Server Version : 50041
File Encoding         : 65001

Date: 2015-07-12 23:29:18
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `ss_role`
-- ----------------------------
DROP TABLE IF EXISTS `ss_role`;
CREATE TABLE `ss_role` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `permissions` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ss_role
-- ----------------------------
INSERT INTO `ss_role` VALUES ('1', 'Admin', 'user:view,user:edit');
INSERT INTO `ss_role` VALUES ('2', 'User', 'user:view');

-- ----------------------------
-- Table structure for `ss_team`
-- ----------------------------
DROP TABLE IF EXISTS `ss_team`;
CREATE TABLE `ss_team` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `master_id` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ss_team
-- ----------------------------
INSERT INTO `ss_team` VALUES ('1', 'Dolphin', '1');

-- ----------------------------
-- Table structure for `ss_user`
-- ----------------------------
DROP TABLE IF EXISTS `ss_user`;
CREATE TABLE `ss_user` (
  `id` bigint(20) NOT NULL auto_increment,
  `login_name` varchar(255) NOT NULL,
  `name` varchar(64) default NULL,
  `password` varchar(255) default NULL,
  `salt` varchar(64) default NULL,
  `email` varchar(128) default NULL,
  `status` varchar(32) default NULL,
  `team_id` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `login_name` (`login_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ss_user
-- ----------------------------
INSERT INTO `ss_user` VALUES ('1', 'admin', '管理员', '691b14d79bf0fa2215f155235df5e670b64394cc', '7efbd59d9741d34f', 'admin@springx.org.cn', 'enabled', '1');
INSERT INTO `ss_user` VALUES ('2', 'user', 'Calvin', '2488aa0c31c624687bd9928e0a5d29e7d1ed520b', '6d65d24122c30500', 'user@springx.org.cn', 'enabled', '1');
INSERT INTO `ss_user` VALUES ('3', 'user2', 'Jack', '2488aa0c31c624687bd9928e0a5d29e7d1ed520b', '6d65d24122c30500', 'jack@springx.org.cn', 'enabled', '1');
INSERT INTO `ss_user` VALUES ('4', 'user3', 'Kate', '2488aa0c31c624687bd9928e0a5d29e7d1ed520b', '6d65d24122c30500', 'kate@springx.org.cn', 'enabled', '1');
INSERT INTO `ss_user` VALUES ('5', 'user4', 'Sawyer', '2488aa0c31c624687bd9928e0a5d29e7d1ed520b', '6d65d24122c30500', 'sawyer@springx.org.cn', 'enabled', '1');
INSERT INTO `ss_user` VALUES ('6', 'user5', 'Ben', '2488aa0c31c624687bd9928e0a5d29e7d1ed520b', '6d65d24122c30500', 'ben@springx.org.cn', 'enabled', '1');

-- ----------------------------
-- Table structure for `ss_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `ss_user_role`;
CREATE TABLE `ss_user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ss_user_role
-- ----------------------------
INSERT INTO `ss_user_role` VALUES ('1', '1');
INSERT INTO `ss_user_role` VALUES ('1', '2');
INSERT INTO `ss_user_role` VALUES ('2', '2');
INSERT INTO `ss_user_role` VALUES ('3', '2');
INSERT INTO `ss_user_role` VALUES ('4', '2');
INSERT INTO `ss_user_role` VALUES ('5', '2');
INSERT INTO `ss_user_role` VALUES ('6', '2');

-- ----------------------------
-- Table structure for `tb_admin`
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `organization_id` bigint(20) default NULL,
  `email` varchar(255) collate utf8_bin NOT NULL,
  `is_enabled` bit(1) NOT NULL,
  `is_locked` bit(1) NOT NULL,
  `locked_date` datetime default NULL,
  `login_date` datetime default NULL,
  `login_failure_count` int(11) NOT NULL,
  `login_ip` varchar(255) collate utf8_bin default NULL,
  `name` varchar(255) collate utf8_bin default NULL,
  `password` varchar(255) collate utf8_bin NOT NULL,
  `username` varchar(100) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_admin
-- ----------------------------
INSERT INTO `tb_admin` VALUES ('1', '2014-11-30 00:03:36', '2015-05-02 11:52:35', '0', 'admin@kcshop.net', '', '\0', null, '2015-05-02 11:52:35', '0', null, '管理员', '2aefc34200a294a3cc7db81b43a81873', 'admin');

-- ----------------------------
-- Table structure for `tb_admin_role`
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin_role`;
CREATE TABLE `tb_admin_role` (
  `id` bigint(20) NOT NULL auto_increment,
  `admin_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_admin_role
-- ----------------------------
INSERT INTO `tb_admin_role` VALUES ('1', '1', '1');

-- ----------------------------
-- Table structure for `tb_organization`
-- ----------------------------
DROP TABLE IF EXISTS `tb_organization`;
CREATE TABLE `tb_organization` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `parent_id` bigint(20) default NULL,
  `parent_ids` varchar(100) default NULL,
  `is_enabled` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_tb_organization_parent_id` (`parent_id`),
  KEY `idx_tb_organization_parent_ids` (`parent_ids`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_organization
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_resource`
-- ----------------------------
DROP TABLE IF EXISTS `tb_resource`;
CREATE TABLE `tb_resource` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `type` varchar(20) default NULL,
  `url` varchar(200) default NULL,
  `level` int(11) unsigned default NULL COMMENT '层级',
  `sort` int(11) default NULL,
  `parent_id` bigint(20) default NULL,
  `permission` varchar(100) default NULL,
  `is_enabled` tinyint(1) default '1',
  PRIMARY KEY  (`id`),
  KEY `idx_tb_resource_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_resource
-- ----------------------------
INSERT INTO `tb_resource` VALUES ('1', '系统管理', 'menu', '', '1', '1', null, 'perms[sys:view]', '1');
INSERT INTO `tb_resource` VALUES ('2', '独立演示', 'menu', '/demo', '1', '2', null, 'perms[demo:view]', '1');
INSERT INTO `tb_resource` VALUES ('3', '资源管理', 'menu', '/resource', '2', null, '1', 'perms[sys:resource]', '1');

-- ----------------------------
-- Table structure for `tb_role`
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role` (
  `id` bigint(20) NOT NULL auto_increment,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `description` varchar(255) collate utf8_bin default NULL,
  `is_system` bit(1) NOT NULL,
  `name` varchar(255) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role` VALUES ('1', '2014-11-30 00:02:44', '2014-11-30 00:02:44', '拥有管理后台最高权限', '', '超级管理员');

-- ----------------------------
-- Table structure for `tb_role_resource`
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_resource`;
CREATE TABLE `tb_role_resource` (
  `id` bigint(20) NOT NULL auto_increment,
  `role_id` bigint(20) NOT NULL,
  `resource_id` bigint(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tb_role_resource
-- ----------------------------
