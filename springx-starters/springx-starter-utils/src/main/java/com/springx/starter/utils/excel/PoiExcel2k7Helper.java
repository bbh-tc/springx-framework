package com.springx.starter.utils.excel;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Excel 读取（2007+新格式）
 * @author	chengesheng
 * @date	2012-4-27 下午03:39:01
 * @note	PoiExcel2k7Helper
 */
public class PoiExcel2k7Helper extends PoiExcelHelper {
	/** 获取sheet列表 
	 * @throws Exception 
	 * @throws java.io.FileNotFoundException */
	public ArrayList<String> getSheetList(String filePath) throws Exception {
	    return getSheetList(new FileInputStream(filePath));
	}
	
	public ArrayList<String> getSheetList(InputStream is) throws Exception {
		ArrayList<String> sheetList = new ArrayList<String>(0);
		XSSFWorkbook wb = new XSSFWorkbook(is);
		Iterator<XSSFSheet> iterator = wb.iterator();
		while (iterator.hasNext()) {
			sheetList.add(iterator.next().getSheetName());
		}
		return sheetList;
	}

	/** 读取Excel文件内容 
	 * @throws Exception 
	 * @throws java.io.FileNotFoundException */
	public ArrayList<ArrayList<String>> readExcel(String filePath, int sheetIndex, String rows, String columns) throws Exception {
		return readExcel(new FileInputStream(filePath),  sheetIndex,  rows, columns);
	}
	
	/** 读取Excel文件内容 
	 * @throws Exception */
	public ArrayList<ArrayList<String>> readExcel(InputStream is, int sheetIndex, String rows, String columns) throws Exception {
		ArrayList<ArrayList<String>> dataList = new ArrayList<ArrayList<String>> ();
		XSSFWorkbook wb = new XSSFWorkbook(is);
		XSSFSheet sheet = wb.getSheetAt(sheetIndex);
		dataList = readExcel(sheet, rows, getColumnNumber(sheet, columns));
		return dataList;
	}
	
	
	
	
	/** 读取Excel文件内容 
	 * @throws Exception 
	 * @throws java.io.FileNotFoundException */
	public ArrayList<ArrayList<String>> readExcel(String filePath, int sheetIndex, String rows, int[] cols) throws FileNotFoundException, Exception {
		return  readExcel(new FileInputStream(filePath),sheetIndex,rows, cols);
	}
	
	/** 读取Excel文件内容 
	 * @throws Exception */
	public ArrayList<ArrayList<String>> readExcel(InputStream is, int sheetIndex, String rows, int[] cols) throws Exception {
		XSSFWorkbook wb = new XSSFWorkbook(is);
		XSSFSheet sheet = wb.getSheetAt(sheetIndex);
		return  readExcel(sheet, rows, cols);
	}
	
	
}