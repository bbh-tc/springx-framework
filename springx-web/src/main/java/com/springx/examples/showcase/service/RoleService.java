package com.springx.examples.showcase.service;

import com.springx.examples.showcase.entity.Role;
import com.springx.examples.showcase.repository.mybatis.RoleMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * Created by roman_000 on 2015/7/15.
 */
@Service
@Transactional
public class RoleService extends  BaseService<Role> {
    public  void  delete(Long[] ids){
        if(ArrayUtils.isNotEmpty(ids)){
            for(Long id:ids){
                Example example = new Example(Role.class);
                example.createCriteria().andEqualTo("id",id).andEqualTo("isSystem","1");
                List<Role> roleList = mapper.selectByExample(example);
                if(CollectionUtils.isNotEmpty(roleList)){
                    throw new IllegalArgumentException("系统内置角色不允许删除");
                }
                mapper.deleteByPrimaryKey(id);
            }
        }
    }
}
