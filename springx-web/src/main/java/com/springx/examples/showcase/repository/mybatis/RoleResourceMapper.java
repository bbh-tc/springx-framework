package com.springx.examples.showcase.repository.mybatis;

import com.springx.examples.showcase.entity.RoleResource;
import tk.mybatis.mapper.common.Mapper;
@MyBatisRepository
public interface RoleResourceMapper extends Mapper<RoleResource> {
}