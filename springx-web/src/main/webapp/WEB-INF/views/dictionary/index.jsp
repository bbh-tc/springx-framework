<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 15-9-8
  Time: 上午10:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <title>数字字典</title>
</head>
<body>

<!--第一行-->
<div class="row">
  <div class="row">
    <div class="col-xs-4" style="min-height: 600px;">
      <ul id="treeView" class="ztree"></ul>
    </div>
    <div class="col-xs-8">
      <!--表单开始-->
      <div class="row">
        <form class="form-horizontal" id="validation-form" style="display:none" method="post">
          <input type="hidden" name="id"/>
          <input type="hidden" name="parentId"/>
          <div class="form-group">
            <label class="control-label col-xs-10 col-sm-2 no-padding-right" for="parentName">上级资源:</label>

            <div class="col-xs-12 col-sm-10">
              <div class="clearfix">
                <input type="text" name="parentName" id="parentName" readonly="readonly" class="form-control-resources col-xs-10 col-sm-6"/>
              </div>
            </div>
          </div>
          <div class="space-2"></div>
          <div class="form-group">
            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="name">资源名称:</label>

            <div class="col-xs-12 col-sm-10">
              <div class="clearfix">
                <input type="text" name="name" id="name" class="col-xs-10 col-sm-6"/>
              </div>
            </div>
          </div>
          <div class="space-2"></div>
          <div class="form-group">
            <label class="control-label col-xs-12 col-sm-2 no-padding-right">资源类型</label>

            <div class="col-xs-12 col-sm-10">
              <div>
                <label class="line-height-1 blue">
                  <input name="type" value="1" type="radio" class="ace" />
                  <span class="lbl"> 菜单</span>
                </label>
              </div>

              <div>
                <label class="line-height-1 blue">
                  <input name="type" value="2" type="radio" class="ace" />
                  <span class="lbl"> 动作</span>
                </label>
              </div>
            </div>
          </div>
          <div class="space-2"></div>
          <div class="form-group">
            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="permissionKey">许可地址:</label>

            <div class="col-xs-12 col-sm-10">
              <div class="clearfix">
                <input type="text" name="permissionKey" id="permissionKey" class="col-xs-10 col-sm-6"/>
                <div class="help-block col-xs-12 col-sm-reset inline">&nbsp;&nbsp;示例：/dictionary/index </div>
              </div>
            </div>
          </div>
          <div class="space-2"></div>
          <div class="form-group">
            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="permissionValue">许可声明:</label>

            <div class="col-xs-12 col-sm-10">
              <div class="clearfix">
                <input type="text" name="permissionValue" id="permissionValue" class="col-xs-10 col-sm-6"/>
                <div class="help-block col-xs-12 col-sm-reset inline">&nbsp;&nbsp;示例：perms[dictionary:view] </div>
              </div>
            </div>
          </div>

          <div class="space-2"></div>
          <div class="form-group">
            <label class="control-label col-xs-12 col-sm-2 no-padding-right" for="url">跳转路径:</label>

            <div class="col-xs-12 col-sm-9">
              <div class="clearfix">
                <textarea class="input-xlarge col-xs-10 col-sm-6" name="url" id="url"></textarea>
              </div>
            </div>
          </div>

          <div class="clearfix">
            <div class="col-md-offset-3 col-md-9">
              <button class="btn btn-info" type="button" id="saveBtn">
                保存
              </button>

              &nbsp; &nbsp; &nbsp;
              <button class="btn" type="button">
                重置
              </button>
            </div>
          </div>
        </form>
      </div>
      <!--表单结束-->
    </div>
  </div>
</div>

<c:import url="../common/ztree.jsp"/>
<c:import url="../common/validate.jsp"/>
<script type="text/javascript" src="${ctx}/resources/ace/dist/js/bootbox.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/module/dictionary/dictionary.js"></script>
</body>
</html>
