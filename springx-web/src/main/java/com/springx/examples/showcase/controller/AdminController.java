package com.springx.examples.showcase.controller;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.springx.examples.showcase.enums.AdminEnum;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.springx.examples.showcase.common.domain.Message;
import com.springx.examples.showcase.entity.Admin;
import com.springx.examples.showcase.service.AdminService;
import com.springx.examples.showcase.vo.DataTableVo;
import com.springx.modules.web.Servlets;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.EnumSet;
import java.util.Map;

/**
 * Created by roman_000 on 2015/7/15.
 */
@Controller
@RequestMapping("/admin")
public class AdminController extends BaseController {
    @Autowired
    private AdminService adminService;

    /**
     * 入口页面
     *
     * @return
     */
    @RequestMapping("/index")
    public String index(Model model) {
        model.addAttribute("enabledItems", EnumSet.allOf(AdminEnum.IS_ENABLED.class));
        model.addAttribute("lockedItems", EnumSet.allOf(AdminEnum.IS_LOCKED.class));
        return "admin/index";
    }

    /**
     * 跳转到edit页面
     *
     * @return
     */
    @RequestMapping("/edit_none")
    public String edit(Long id, Model model) {
        if (null != id) {
            Admin admin = adminService.select(id);
            model.addAttribute("admin", admin);
        }
        return "admin/edit";
    }

    /**
     * 列表页面
     *
     * @param request
     * @return
     * @throws ServletRequestBindingException
     */
    @RequestMapping("/list")
    public
    @ResponseBody
    DataTableVo<Admin> list(HttpServletRequest request) throws ServletRequestBindingException {
        Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
        PageRequest pageRequest = initPageRequest(request);
        Page<Admin> page = adminService.selectPage(searchParams, null, pageRequest);
        DataTableVo<Admin> dataTableVo = new DataTableVo(request, page);
        return dataTableVo;
    }

    @RequestMapping(value = "/insert")
    public
    @ResponseBody
    Message insert(@Valid Admin admin) {
        admin.setCreateDate(new Date());
        admin.setIsLocked(false);
        admin.setIsEnabled(true);
        admin.setLoginFailureCount(0);
        adminService.insert(admin);
        Message message = new Message(Message.StatusType.SUCCESS, "保存成功");
        return message;
    }

    @RequestMapping("/delete")
    public
    @ResponseBody
    Message delete(@RequestParam(value = "ids[]") Long[] ids) {
        adminService.delete(ids);
        Message message = new Message(Message.StatusType.SUCCESS, "删除成功");
        return message;
    }

    @RequestMapping("/update")
    public
    @ResponseBody
    Message update(@Valid Admin admin) {
        Assert.notNull(admin.getId(), "参数错误");
        Admin editAdmin = adminService.select(admin.getId());
        if (editAdmin != null) {
            editAdmin.setIsEnabled(null == admin.getIsEnabled() ? false : admin.getIsEnabled());
            editAdmin.setName(admin.getName());
            editAdmin.setEmail(admin.getEmail());
            editAdmin.setOrganizationId(admin.getOrganizationId());
            editAdmin.setIsLocked(null == admin.getIsLocked() ? false : admin.getIsLocked());
            editAdmin.setModifyDate(new Date());
            adminService.updateByPrimaryKeySelective(editAdmin);
        }

        Message message = new Message(Message.StatusType.SUCCESS, "修改成功");
        return message;
    }


}
