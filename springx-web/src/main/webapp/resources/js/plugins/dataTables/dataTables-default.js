if ($.fn.dataTable.Api) {
    $.fn.dataTable.defaults.renderer = 'bootstrap';
    $.fn.dataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
        var api = new $.fn.dataTable.Api(settings);
        var classes = settings.oClasses;
        var lang = settings.oLanguage.oPaginate;
        var btnDisplay, btnClass;
        var attach = function (container, buttons) {
            var i, ien, node, button;
            var clickHandler = function (e) {
                e.preventDefault();
                if (e.data.action !== 'ellipsis') {
                    api.page(e.data.action).draw(false);
                }
            };

            for (i = 0, ien = buttons.length; i < ien; i++) {
                button = buttons[i];

                if ($.isArray(button)) {
                    attach(container, button);
                }
                else {
                    btnDisplay = '';
                    btnClass = '';

                    switch (button) {
                        case 'ellipsis':
                            btnDisplay = '&hellip;';
                            btnClass = 'disabled';
                            break;

                        case 'first':
                            btnDisplay = lang.sFirst;
                            btnClass = button + (page > 0 ?
                                '' : ' disabled');
                            break;

                        case 'previous':
                            btnDisplay = lang.sPrevious;
                            btnClass = button + (page > 0 ?
                                '' : ' disabled');
                            break;

                        case 'next':
                            btnDisplay = lang.sNext;
                            btnClass = button + (page < pages - 1 ?
                                '' : ' disabled');
                            break;

                        case 'last':
                            btnDisplay = lang.sLast;
                            btnClass = button + (page < pages - 1 ?
                                '' : ' disabled');
                            break;

                        default:
                            btnDisplay = button + 1;
                            btnClass = page === button ?
                                'active' : '';
                            break;
                    }

                    if (btnDisplay) {
                        node = $('<li>', {
                            'class': classes.sPageButton + ' ' + btnClass,
                            'aria-controls': settings.sTableId,
                            'tabindex': settings.iTabIndex,
                            'id': idx === 0 && typeof button === 'string' ?
                            settings.sTableId + '_' + button :
                                null
                        })
                            .append($('<a>', {
                                'href': '#'
                            })
                                .html(btnDisplay)
                        )
                            .appendTo(container);

                        settings.oApi._fnBindAction(
                            node, {action: button}, clickHandler
                        );
                    }
                }
            }
        };

        attach(
            $(host).empty().html('<ul class="pagination"/>').children('ul'),
            buttons
        );
    }
}
if (typeof dataTableJs == 'undefined') {
    dataTableJs = {};
    dataTableJs.activeClass = 'active';
    dataTableJs.default = {
        "dom": 't<"row"<"col-xs-12"lpri>>',
        "language": {
            "sProcessing": "处理中...",
            "sLengthMenu": "_MENU_",
            "sZeroRecords": "没有匹配结果",
            "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
            "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
            "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
            "sInfoPostFix": "",
            "sSearch": "筛选:",
            "sUrl": "",
            "sEmptyTable": "没有查询到数据",
            "sLoadingRecords": "载入中...",
            "sInfoThousands": ",",
            "oPaginate": {
                "sFirst": "首页",
                "sPrevious": "上页",
                "sNext": "下页",
                "sLast": "末页"
            },
            "oAria": {
                "sSortAscending": ": 以升序排列此列",
                "sSortDescending": ": 以降序排列此列"
            }
        },
        "processing": true,
        "serverSide": true,
        "bAutoWidth": false/**自动调整大小*/
    }
    dataTableJs.buttonTemplate = function (obj) {
        return '<a href="javascript:void(0)" onclick="'+obj.fn+'"><span class="label label-xlg label-' + obj.type + ' arrowed-in">' + obj.name + '</span></a>';
    }
    dataTableJs.buttonGroupTemplate = function (ary) {
        var html = "";
        if (ary && ary.length > 0) {
            for (i = 0; i < ary.length; i++) {
                html += dataTableJs.buttonTemplate(ary[i]) + "&nbsp;";
            }
        }
        return "<div class='center'>" + html + "</div>";
    }
    dataTableJs.doCheck = function (tableId) {
        tableId = tableId || dataTableJs.tableId;
        var active_class = dataTableJs.activeClass;
        $("#" + tableId + ' > thead > tr > th input[type=checkbox]').eq(0).on('click', function () {
            var th_checked = this.checked;//checkbox inside "TH" table header
            $(this).closest('table').find('tbody > tr').each(function () {
                var row = this;
                if (th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
            });
        });
    }
    dataTableJs.getChecks = function (tableId) {
        tableId = tableId || dataTableJs.tableId;
        return $("#" + tableId + " > tbody > tr").find("input[type=checkbox]:checked");
    }
    dataTableJs.selectRow = function (tableId) {
        tableId = tableId || dataTableJs.tableId;
        $('#' + tableId).on('click', 'td input[type=checkbox]', function () {
            var $row = $(this).closest('tr');
            if (this.checked) $row.addClass(dataTableJs.activeClass);
            else $row.removeClass(dataTableJs.activeClass);
        });
    }
    dataTableJs.serialize =function (params,selector) {
        params = params ? $.param(params) + "&" : "";
        return params+$(selector).serialize();
    }
}

$(function () {
    dataTableJs.tableId = dataTableJs.tableId || $(".table", ".main-content-inner").eq(0).attr("id");
    if (!dataTableJs.tableId) {
        $.message.alert({title: "信息提示", message: "请先初始化表格ID", size: 'small'});
        return;
    }
    //全选反选
    dataTableJs.doCheck();
    dataTableJs.selectRow();
})
