<%@ page import="org.apache.shiro.subject.Subject" %>
<%@ page import="org.apache.shiro.SecurityUtils" %>
<%@ page import="com.springx.shiro.domain.Principal" %>
<%@ page import="com.springx.shiro.domain.ShiroResource" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.apache.commons.collections.MapUtils" %>
<%@ page import="org.apache.commons.collections.CollectionUtils" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!-- #section:basics/sidebar -->
<div id="sidebar" class="sidebar                  responsive">
    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
        function loadMainContent(url) {
            try {
               $(".custom-tab0").html("加载中......").load(url);
            } catch (e) {

            }
        }
    </script>


    <ul class="nav nav-list">
        <li menu="menu_x">
            <a href="${ctx}">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text">系统首页</span>
            </a>

            <b class="arrow"></b>
        </li>


        <%


            Subject subject = SecurityUtils.getSubject();
            if (subject != null) {
                Principal principal = (Principal) subject.getPrincipal();
                if (principal != null) {
                    Map<Long, List<ShiroResource>> menus = principal.getMenus();
                    if (MapUtils.isNotEmpty(menus)) {
                        List<ShiroResource> shiroResoureList = menus.get(-1l);
                        if (CollectionUtils.isNotEmpty(shiroResoureList)) {
                            List<ShiroResource> childrenShiroResource;
                            for (ShiroResource resource : shiroResoureList) {
        %>
        <li menu="menu_<%=resource.getId()%>">
            <a href="#" class="dropdown-toggle" >
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"><%=resource.getName()%></span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <%
                childrenShiroResource = menus.get(resource.getId());
                if (CollectionUtils.isNotEmpty(childrenShiroResource)) {
            %>
            <ul class="submenu">
                <%
                    for (ShiroResource resource1 : childrenShiroResource) {
                %>
                <li menu="menu_<%=resource1.getId()%>">
                    <a  href="${ctx}<%=resource1.getUrl()%>?menuId=<%=resource1.getId()%>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        <%=resource1.getName()%>
                    </a>

                    <b class="arrow"></b>
                </li>
                <% } %>
            </ul>
            <%
                }
            %>
        </li>
        <%
                            }
                        }
                    }
                }
            }
        %>


        <%-- <li class="active open">
            <a href="#" class="dropdown-toggle">
                 <i class="menu-icon fa fa-list"></i>
                 <span class="menu-text">系统管理 </span>

                 <b class="arrow fa fa-angle-down"></b>
             </a>

             <b class="arrow"></b>

             <ul class="submenu">
                 <li class="active">
                     <a href="${ctx}/admin/index">
                         <i class="menu-icon fa fa-caret-right"></i>
                         用户管理
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="${ctx}/resource/index">
                         <i class="menu-icon fa fa-caret-right"></i>
                         资源管理
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="${ctx}/role/index">
                         <i class="menu-icon fa fa-caret-right"></i>
                         角色管理
                     </a>

                     <b class="arrow"></b>
                 </li>
                 <li class="">
                     <a href="${ctx}/organization/index">
                         <i class="menu-icon fa fa-caret-right"></i>
                         机构管理
                     </a>

                     <b class="arrow"></b>
                 </li>
             </ul>
         </li>


         <li class="">
             <a href="#" class="dropdown-toggle">
                 <i class="menu-icon fa fa-desktop"></i>
                             <span class="menu-text">
                                 UI &amp; Elements
                             </span>

                 <b class="arrow fa fa-angle-down"></b>
             </a>

             <b class="arrow"></b>

             <ul class="submenu">
                 <li class="">
                     <a href="#" class="dropdown-toggle">
                         <i class="menu-icon fa fa-caret-right"></i>

                         Layouts
                         <b class="arrow fa fa-angle-down"></b>
                     </a>

                     <b class="arrow"></b>

                     <ul class="submenu">
                         <li class="">
                             <a href="top-menu.html">
                                 <i class="menu-icon fa fa-caret-right"></i>
                                 Top Menu
                             </a>

                             <b class="arrow"></b>
                         </li>

                         <li class="">
                             <a href="two-menu-1.html">
                                 <i class="menu-icon fa fa-caret-right"></i>
                                 Two Menus 1
                             </a>

                             <b class="arrow"></b>
                         </li>

                         <li class="">
                             <a href="two-menu-2.html">
                                 <i class="menu-icon fa fa-caret-right"></i>
                                 Two Menus 2
                             </a>

                             <b class="arrow"></b>
                         </li>

                         <li class="">
                             <a href="mobile-menu-1.html">
                                 <i class="menu-icon fa fa-caret-right"></i>
                                 Default Mobile Menu
                             </a>

                             <b class="arrow"></b>
                         </li>

                         <li class="">
                             <a href="mobile-menu-2.html">
                                 <i class="menu-icon fa fa-caret-right"></i>
                                 Mobile Menu 2
                             </a>

                             <b class="arrow"></b>
                         </li>

                         <li class="">
                             <a href="mobile-menu-3.html">
                                 <i class="menu-icon fa fa-caret-right"></i>
                                 Mobile Menu 3
                             </a>

                             <b class="arrow"></b>
                         </li>
                     </ul>
                 </li>

                 <li class="">
                     <a href="typography.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Typography
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="elements.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Elements
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="buttons.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Buttons &amp; Icons
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="content-slider.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Content Sliders
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="treeview.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Treeview
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="jquery-ui.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         jQuery UI
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="nestable-list.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Nestable Lists
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="#" class="dropdown-toggle">
                         <i class="menu-icon fa fa-caret-right"></i>

                         Three Level Menu
                         <b class="arrow fa fa-angle-down"></b>
                     </a>

                     <b class="arrow"></b>

                     <ul class="submenu">
                         <li class="">
                             <a href="#">
                                 <i class="menu-icon fa fa-leaf green"></i>
                                 Item #1
                             </a>

                             <b class="arrow"></b>
                         </li>

                         <li class="">
                             <a href="#" class="dropdown-toggle">
                                 <i class="menu-icon fa fa-pencil orange"></i>

                                 4th level
                                 <b class="arrow fa fa-angle-down"></b>
                             </a>

                             <b class="arrow"></b>

                             <ul class="submenu">
                                 <li class="">
                                     <a href="#">
                                         <i class="menu-icon fa fa-plus purple"></i>
                                         Add Product
                                     </a>

                                     <b class="arrow"></b>
                                 </li>

                                 <li class="">
                                     <a href="#">
                                         <i class="menu-icon fa fa-eye pink"></i>
                                         View Products
                                     </a>

                                     <b class="arrow"></b>
                                 </li>
                             </ul>
                         </li>
                     </ul>
                 </li>
             </ul>
         </li>

         <li class="active open">
             <a href="#" class="dropdown-toggle">
                 <i class="menu-icon fa fa-list"></i>
                 <span class="menu-text"> Tables </span>

                 <b class="arrow fa fa-angle-down"></b>
             </a>

             <b class="arrow"></b>

             <ul class="submenu">
                 <li class="active">
                     <a href="tables.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Simple &amp; Dynamic
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="jqgrid.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         jqGrid plugin
                     </a>

                     <b class="arrow"></b>
                 </li>
             </ul>
         </li>

         <li class="">
             <a href="#" class="dropdown-toggle">
                 <i class="menu-icon fa fa-pencil-square-o"></i>
                 <span class="menu-text"> Forms </span>

                 <b class="arrow fa fa-angle-down"></b>
             </a>

             <b class="arrow"></b>

             <ul class="submenu">
                 <li class="">
                     <a href="form-elements.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Form Elements
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="form-elements-2.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Form Elements 2
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="form-wizard.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Wizard &amp; Validation
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="wysiwyg.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Wysiwyg &amp; Markdown
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="dropzone.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Dropzone File Upload
                     </a>

                     <b class="arrow"></b>
                 </li>
             </ul>
         </li>

         <li class="">
             <a href="widgets.html">
                 <i class="menu-icon fa fa-list-alt"></i>
                 <span class="menu-text"> Widgets </span>
             </a>

             <b class="arrow"></b>
         </li>

         <li class="">
             <a href="calendar.html">
                 <i class="menu-icon fa fa-calendar"></i>

                             <span class="menu-text">
                                 Calendar

                                 <!-- #section:basics/sidebar.layout.badge -->
                                 <span class="badge badge-transparent tooltip-error" title="2 Important Events">
                                     <i class="ace-icon fa fa-exclamation-triangle red bigger-130"></i>
                                 </span>

                                 <!-- /section:basics/sidebar.layout.badge -->
                             </span>
             </a>

             <b class="arrow"></b>
         </li>

         <li class="">
             <a href="gallery.html">
                 <i class="menu-icon fa fa-picture-o"></i>
                 <span class="menu-text"> Gallery </span>
             </a>

             <b class="arrow"></b>
         </li>

         <li class="">
             <a href="#" class="dropdown-toggle">
                 <i class="menu-icon fa fa-tag"></i>
                 <span class="menu-text"> More Pages </span>

                 <b class="arrow fa fa-angle-down"></b>
             </a>

             <b class="arrow"></b>

             <ul class="submenu">
                 <li class="">
                     <a href="profile.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         User Profile
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="inbox.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Inbox
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="pricing.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Pricing Tables
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="invoice.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Invoice
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="timeline.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Timeline
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="email.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Email Templates
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="login.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Login &amp; Register
                     </a>

                     <b class="arrow"></b>
                 </li>
             </ul>
         </li>

         <li class="">
             <a href="#" class="dropdown-toggle">
                 <i class="menu-icon fa fa-file-o"></i>

                             <span class="menu-text">
                                 Other Pages

                                 <!-- #section:basics/sidebar.layout.badge -->
                                 <span class="badge badge-primary">5</span>

                                 <!-- /section:basics/sidebar.layout.badge -->
                             </span>

                 <b class="arrow fa fa-angle-down"></b>
             </a>

             <b class="arrow"></b>

             <ul class="submenu">
                 <li class="">
                     <a href="faq.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         FAQ
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="error-404.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Error 404
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="error-500.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Error 500
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="grid.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Grid
                     </a>

                     <b class="arrow"></b>
                 </li>

                 <li class="">
                     <a href="blank.html">
                         <i class="menu-icon fa fa-caret-right"></i>
                         Blank Page
                     </a>

                     <b class="arrow"></b>
                 </li>
             </ul>
         </li>--%>
    </ul>
    <!-- /.nav-list -->

    <!-- #section:basics/sidebar.layout.minimize -->
    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left"
           data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>

    <!-- /section:basics/sidebar.layout.minimize -->
    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }
        $(function (){
            if('${param.menuId}'){
                var menu= $("li[menu='menu_${param.menuId}']");
                menu.addClass("active");
                menu.parents("li").addClass("open");
            }

        })
    </script>
</div>

<!-- /section:basics/sidebar -->