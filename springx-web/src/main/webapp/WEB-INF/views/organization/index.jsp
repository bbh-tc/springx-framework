<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>资源管理</title>
</head>
<body>
<!--第一行-->
<div class="row">
    <div class="row">
        <div class="col-xs-4" style="min-height: 600px;">
            <ul id="treeView" class="ztree"></ul>
        </div>
        <div class="col-xs-4">
            <!--表单开始-->
            <div class="row">
                <form class="form-horizontal" id="validation-form" style="display:none" method="post">
                    <input type="hidden" name="id"/>
                    <input type="hidden" name="parentId"/>
                    <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 no-padding-right" for="parentName">上级机构:</label>

                        <div class="col-xs-12 col-sm-8">
                                <input type="text" name="parentName" id="parentName" readonly="readonly" class="form-control-resources form-control"/>
                        </div>
                    </div>
                    <div class="space-2"></div>
                    <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 no-padding-right" for="name">机构名称:</label>

                        <div class="col-xs-12 col-sm-8">
                                <input type="text" name="name" id="name" class="form-control"/>
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 no-padding-right">设置</label>

                        <div class="col-xs-12 col-sm-8 checkbox-inline">
                            <label>
                                <input name="isEnabled" value="1" id="isEnabled" type="checkbox" class="ace"/>
                                <span class="lbl"> 启用</span>
                            </label>
                        </div>
                    </div>

                    <div class="space-2"></div>
                    <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 no-padding-right">说明:</label>

                        <div class="col-xs-12 col-sm-8">
                                <textarea class="form-control" name="description" id="description"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-offset-4 col-sm-offset-6">
                            <button class="btn btn-info" type="button" id="saveBtn">
                                保存
                            </button>

                            &nbsp; &nbsp; &nbsp;
                            <button class="btn" type="button" id="resetBtn">
                                重置
                            </button>
                        </div>
                    </div>
                </form>
            </div>
           <!--表单结束-->
        </div>
    </div>
</div>

<c:import url="../common/ztree.jsp"/>
<c:import url="../common/validate.jsp"/>
<script type="text/javascript" src="${ctx}/resources/js/module/organization/organization.js"></script>
</body>
</html>
