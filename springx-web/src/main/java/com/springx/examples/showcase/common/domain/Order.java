package com.springx.examples.showcase.common.domain;

import java.io.Serializable;

/**
 * Created by roman_000 on 2015/8/10.
 */
public class Order implements Serializable {
    public static final String ASC = "asc";
    public static final String DESC = "desc";
    public static final String PROPERTY_NAME="order";
    private String property;
    private String direction;

    public Order(String property, String direction) {
        this.property = property;
        this.direction = direction;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
