package com.springx.examples.showcase.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.springx.examples.showcase.entity.Admin;

/**
 * Created by roman_000 on 2015/7/15.
 */
@Service
@Transactional
public class AdminService extends  BaseService<Admin> {
}
