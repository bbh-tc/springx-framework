//请指定操作的表格Id,以免多次指定
dataTableJs.tableId = 'dynamic-table';
var oTable1;
$(function () {
    oTable1 =
        $('#' + dataTableJs.tableId).dataTable($.extend(dataTableJs.default, {
            "ajax": {
                url: ctx + "/role/list", data: function (d) {
                    delete d.columns;
                    //增加page和pageSize
                    if (oTable1) {
                        d._page = oTable1.api().page();
                        d._pageSize = oTable1.api().page.len();
                    }
                    //增加查询
                    return dataTableJs.serialize(d, "#search-form");
                }
            },
            "order": [[4, "desc"]], /**默认排序*/
            "columns": [
                {"data": null, orderable: false},
                {"data": "name", orderable: false},
                {"data": "isSystem", orderable: false},
                {"data": "description", orderable: false},
                {"data": "createDate"},
                {"data": null, orderable: false}
            ],
            columnDefs: [
                {
                    targets: 0,
                    render: function (a, b, c, d) {
                        var result = "";
                        if (!c.isSystem) {
                            result = '<div class="center"><label class="pos-rel"><input type="checkbox" class="ace" value="' + c.id + '"> <span class="lbl"></span></label></div>';
                        }
                        return result;
                    }
                },
                {
                    targets: 2,
                    render: function (a, b, c, d) {
                        return a ? "是" : "否";
                    }
                },
                {
                    targets: 5,
                    render: function (a, b, c, d) {
                        return dataTableJs.buttonGroupTemplate([
                            {
                                "name": "编辑",
                                "fn": "doEdit(" + c.id + ")",
                                "type": "purple"
                            },
                            {
                                "name": "授权",
                                "fn": "doAuthorization(" + c.id + ")",
                                "type": "warning"
                            }
                        ]);
                    }
                }

            ]
        }));
})

function formCheck() {
    return $('#editForm').validate({
        rules: {
            name: {
                required: true
            },
            description: {
                rangelength: [0, 200]
            }
        }
    });
}

function buildButton(type) {
    var btnTemplate = {
        className: "btn-success"
    };
    //btn1 增加保存  btn1 编辑保存  btn2 授权保存
    var labelObject = {btn0: "关闭", "btn1": "保存", "btn2": "保存", "btn3": "保存"};
    btnTemplate.label = labelObject[type];
    btnTemplate.callback = function () {
        if ("btn0" == type) {
            return true;//关闭
        }
        if ("btn0" == type || "btn1" == type) {
            if (!formCheck().form()) {
                return false;
            }
            var $form = $("#editForm");
            if ("btn1" == type) {//增加界面保存
                $.getJSON(ctx + "/role/insert", $form.serialize(), function (data) {
                    $.messager.alert0({selector: "#editAlert", type: data.status, html: data.body});
                    if (data && data.status == "SUCCESS") {
                        oTable1.api().ajax.reload();
                        $.bootbox.hideAll();
                    }
                });
            } else if ("btn2" == type) {//修改界面保存
                $.getJSON(ctx + "/role/update", nodeIds, function (data) {
                    $.messager.alert0({selector: "#editAlert", type: data.status, html: data.body});
                    if (data && data.status == "SUCCESS") {
                        oTable1.api().ajax.reload();
                        $.bootbox.hideAll();
                    }
                });
            }
        }
        //授权界面保存
        if ("btn3" == type) {
            var nodes = authorizationJs.getChangeCheckedNodes();
            var nodeIds = [];
            if (nodes && nodes.length > 0) {
                $.each(nodes, function (i, node) {
                    nodeIds.push({id: node.id, checked: node.checked});
                });
            }
            var roleId = $("input[name=roleId]").val();
            var url=ctx + "/role/authorization/insert";
            $.ajax({
                type: "POST",
                url: url,
                data: {roleId:roleId,checks:JSON.stringify(nodeIds)},//将对象序列化成JSON字符串
                dataType:"json",
                success: function(data){
                    $.messager.alert0({selector: "#editAlert", type: data.status, html: data.body});
                    if (data && data.status == "SUCCESS") {
                        $.bootbox.hideAll();
                    }
                }
            });
        }
        return false;
    }
    return btnTemplate;

}

function doEdit(id) {
    var url = ctx + "/role/edit_none?id=" + id;
    var title = "用户编辑";
    var buttons = {
        btn0: buildButton("btn2"),//编辑时候的保存按钮
        btn1: buildButton("btn0")
    };
    $.messager.dialog({url: url, title: title, buttons: buttons});
}

function doAdd() {
    var url = ctx + "/role/edit_none";
    var title = "角色增加";
    var buttons = {
        btn1: buildButton("btn1"),
        btn0: buildButton("btn0")
    };
    $.messager.dialog({url: url, title: title, buttons: buttons});
}
/**
 *删除
 */
function doDelete() {
    var cks = dataTableJs.getChecks();
    if (cks.size() == 0) {
        $.messager.alert("请选择需要删除的数据项");
        return;
    }
    var url = ctx + "/role/delete";
    $.messager.confirm("你确定要删除选中的记录吗?", function (ck) {
        if (ck) {
            var ary = [];
            $.each(cks, function (i) {
                ary.push(this.value);
            });
            $.getJSON(url, {ids: ary}, function (data) {
                $.messager.alert0({selector: "#indexAlert", type: data.status, html: data.body});
                if (data && data.status == 'SUCCESS') {
                    oTable1.api().ajax.reload();
                }
            });
        }
    });
}
/**查询*/
function doSearch() {
    oTable1.api().ajax.data = {"params": $("#search-form").serialize()};
    oTable1.api().ajax.reload();
}

function doAuthorization(id) {
    var url = ctx + "/role/authorization_none?id=" + id;
    var title = "角色授权";
    var buttons = {
        btn3: buildButton("btn3"),
        btn0: buildButton("btn0")
    };
    $.messager.dialog({url: url, title: title, buttons: buttons});
}

