package com.springx.starter.utils.excel;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * Excel 读取（97-2003格式）
 * @author	tancheng
 * @date	2012-4-27 下午03:39:01
 * @note	PoiExcel2k3Helper
 */
public class PoiExcel2k3Helper extends PoiExcelHelper {
	/** 获取sheet列表 
	 * @throws Exception 
	 * @throws java.io.FileNotFoundException */
	public ArrayList<String> getSheetList(String filePath) throws Exception {
		return getSheetList(new FileInputStream(filePath)) ;
	}
	
	public ArrayList<String> getSheetList(InputStream is) throws Exception {
		ArrayList<String> sheetList = new ArrayList<String>(0);
		HSSFWorkbook wb = new HSSFWorkbook(is);
		int i = 0;
		while (true) {
			try {
				String name = wb.getSheetName(i);
				sheetList.add(name);
				i++;
			} catch (Exception e) {
				break;
			}
		}
		return sheetList;
	}

	/** 读取Excel文件内容 
	 * @throws Exception 
	 * @throws Exception */
	public ArrayList<ArrayList<String>> readExcel(String filePath, int sheetIndex, String rows, String columns) throws Exception {
		return readExcel(new FileInputStream(filePath),sheetIndex,rows,columns);
	}
	
	/** 读取Excel文件内容 
	 * @throws Exception */
	public ArrayList<ArrayList<String>> readExcel(InputStream is, int sheetIndex, String rows, String columns) throws Exception {
			HSSFWorkbook wb = new HSSFWorkbook(is);
			HSSFSheet sheet = wb.getSheetAt(sheetIndex);
			return  readExcel(sheet, rows, getColumnNumber(sheet, columns));
	}
	
	
	
	/** 读取Excel文件内容 
	 * @throws Exception 
	 * @throws java.io.FileNotFoundException */
	public ArrayList<ArrayList<String>> readExcel(String filePath, int sheetIndex, String rows, int[] cols) throws Exception {
		return readExcel(new FileInputStream(filePath),sheetIndex,  rows, cols);
	}
	
	
	/** 读取Excel文件内容 
	 * @throws Exception */
	public ArrayList<ArrayList<String>> readExcel(InputStream is, int sheetIndex, String rows, int[] cols) throws Exception {
		HSSFWorkbook wb = new HSSFWorkbook(is);
		HSSFSheet sheet = wb.getSheetAt(sheetIndex);
		return  readExcel(sheet, rows, cols);
	}
}