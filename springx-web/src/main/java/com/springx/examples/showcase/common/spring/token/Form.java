package com.springx.examples.showcase.common.spring.token;
import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;
/**
 * 表单类
 *
 * @author DigitalSonic
 */
public class Form implements Serializable {
    /**
     * serialVersionUID
     */
    private static final long   serialVersionUID        = 8796758608626021150L;
    public static final String  FORM_TOKEN_FIELD_NAME = "form_token";
    /** 表单标识*/
    private String              token;
    /** 表单创建时间*/
    private Date                createTime;
    /**
     * 构造函数
     */
    public Form(String token) {
        this.token = token;
        this.createTime = new Date();
    }
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
    public String getToken() {
        return token;
    }
    public Date getCreateTime() {
        return createTime;
    }
}