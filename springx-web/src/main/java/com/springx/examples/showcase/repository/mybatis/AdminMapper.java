package com.springx.examples.showcase.repository.mybatis;

import com.springx.examples.showcase.entity.Admin;
import tk.mybatis.mapper.common.Mapper;

/**
 * Created by roman_000 on 2015/7/16.
 */
@MyBatisRepository
public interface AdminMapper extends Mapper<Admin>{
}
