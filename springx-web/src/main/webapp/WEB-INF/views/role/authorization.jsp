<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<body>
<input type="hidden" name="roleId" value="${role.id}"/>
<div class="row">
    <div class="col-xs-10" style="min-height: 600px;">
        <ul id="treeView" class="ztree"></ul>
    </div>
</div>
<div id="editAlert" class="hide" style="min-width:550px"></div>
<c:import url="../common/ztree.jsp"/>
<script type="text/javascript" src="${ctx}/resources/js/module/role/authorization.js"></script>
</body>
</html>