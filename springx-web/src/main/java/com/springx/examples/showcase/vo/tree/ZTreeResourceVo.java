package com.springx.examples.showcase.vo.tree;

/**
 * Created by roman_000 on 2015/7/12.
 */
public class ZTreeResourceVo extends ZTreeVo {

    private String permissionKey;
    private String permissionValue;

    public String getPermissionKey() {
        return permissionKey;
    }

    public void setPermissionKey(String permissionKey) {
        this.permissionKey = permissionKey;
    }

    public String getPermissionValue() {
        return permissionValue;
    }

    public void setPermissionValue(String permissionValue) {
        this.permissionValue = permissionValue;
    }
}
