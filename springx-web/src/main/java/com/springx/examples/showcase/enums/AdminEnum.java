package com.springx.examples.showcase.enums;

import com.google.common.collect.Maps;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by roman_000 on 2015/8/8.
 */
public class AdminEnum {
    public  enum  IS_ENABLED{
        ENABLED("1","启用"),
        DISABLED("0","禁用");

        private  String  key;
        private  String    value;

        IS_ENABLED(String key,String value){
            this.key=key;
            this.value=value;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public  enum  IS_LOCKED{
        LOCK("1","锁定"),
        UNLOCK("0","正常");

        private  String  key;
        private  String    value;

        IS_LOCKED(String key,String value){
            this.key=key;
            this.value=value;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
