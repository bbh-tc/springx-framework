<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<link rel="stylesheet" href="${ctx}/resources/js/plugins/jquery-validation/validate.css"/>
<script src="${ctx}/resources/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="${ctx}/resources/js/plugins/jquery-validation/jquery.validate.extend.js"></script>