package com.springx.examples.showcase.repository.mybatis;


import com.springx.examples.showcase.entity.Dictionary;
import tk.mybatis.mapper.common.Mapper;


@MyBatisRepository
public interface DictionaryMapper extends Mapper<Dictionary>{
}
