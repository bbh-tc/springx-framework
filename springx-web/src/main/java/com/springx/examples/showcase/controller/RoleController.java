package com.springx.examples.showcase.controller;

import com.fasterxml.jackson.databind.JavaType;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.springx.examples.showcase.common.domain.Message;
import com.springx.examples.showcase.common.domain.Order;
import com.springx.examples.showcase.common.spring.HtmlCleanEditor;
import com.springx.examples.showcase.entity.Role;
import com.springx.examples.showcase.entity.RoleResource;
import com.springx.examples.showcase.service.ResourceService;
import com.springx.examples.showcase.service.RoleResourceService;
import com.springx.examples.showcase.service.RoleService;
import com.springx.examples.showcase.vo.DataTableVo;
import com.springx.examples.showcase.vo.tree.ZTreeResourceVo;
import com.springx.examples.showcase.vo.tree.ZTreeRootVo;
import com.springx.examples.showcase.vo.tree.ZTreeVo;
import com.springx.modules.mapper.JsonMapper;
import com.springx.modules.web.Servlets;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

/**
 * Created by roman_000 on 2015/7/15.
 */
@Controller
@RequestMapping("/role")
public class RoleController extends BaseController {
    @Autowired
    private RoleService roleService;
    @Autowired
    private RoleResourceService roleResourceService;
    @Autowired
    private ResourceService resourceService;

    /**
     * 入口页面
     *
     * @return
     */
    @RequestMapping("/index")
    public String index(Model model) {

        return "role/index";
    }

    /**
     * 入口页面
     *
     * @return
     */
    @RequestMapping("/authorization_none")
    public String authorizationIndex(Long id, Model model) {
        if (null != id) {
            Role role = roleService.select(id);
            model.addAttribute("role", role);
        }
        return "role/authorization";
    }

    /**
     * 跳转到edit页面
     *
     * @return
     */
    @RequestMapping("/edit_none")
    public String edit(Long id, Model model) {
        if (null != id) {
            Role role = roleService.select(id);
            model.addAttribute("role", role);
        }
        return "role/edit";
    }


    /**
     * 列表页面
     *
     * @param request
     * @return
     * @throws org.springframework.web.bind.ServletRequestBindingException
     */
    @RequestMapping("/list")
    public
    @ResponseBody
    DataTableVo<Role> list(HttpServletRequest request) throws ServletRequestBindingException {
        Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
        PageRequest pageRequest = initPageRequest(request);
        Page<Role> page = roleService.selectPage(searchParams, null, pageRequest);
        DataTableVo<Role> dataTableVo = new DataTableVo(request, page);
        ;
        return dataTableVo;
    }

    @RequestMapping(value = "/insert")
    public
    @ResponseBody
    Message insert(@Valid Role role) {
        role.setCreateDate(new Date());
        role.setIsSystem(false);
        roleService.insert(role);
        Message message = new Message(Message.StatusType.SUCCESS, "保存成功");
        return message;
    }

    @RequestMapping("/delete")
    public
    @ResponseBody
    Message delete(@RequestParam(value = "ids[]") Long[] ids) {
        roleService.delete(ids);
        Message message = new Message(Message.StatusType.SUCCESS, "删除成功");
        return message;
    }


    @RequestMapping("/update")
    public
    @ResponseBody
    Message update(@Valid Role role) {
        Assert.notNull(role.getId(), "参数错误");
        Role editRole = roleService.select(role.getId());
        if (editRole != null) {
            Assert.isTrue(!editRole.getIsSystem(), "内置角色不允许编辑");
            editRole.setName(role.getName());
            editRole.setDescription(role.getDescription());
            editRole.setModifyDate(new Date());
            roleService.updateByPrimaryKeySelective(editRole);
        }
        Message message = new Message(Message.StatusType.SUCCESS, "修改成功");
        return message;
    }

    /**
     * 授权信息保存
     *
     * @param roleId checks
     * @return
     */
    @RequestMapping(value = "/authorization/insert")
    public
    @ResponseBody
    Message authorizationInsert(Long roleId, String checks) {
        Assert.notNull(roleId, "参数错误");
        if (StringUtils.isNotBlank(checks)) {
            JsonMapper mapper = JsonMapper.nonEmptyMapper();
            JavaType beanListType = mapper.contructCollectionType(List.class, HashMap.class);
            String result=StringEscapeUtils.unescapeHtml4(checks);
            List<HashMap<String,Object>> checkParams = mapper.fromJson(result, beanListType);
            roleResourceService.insert(roleId,checkParams);
        }
        Message message = new Message(Message.StatusType.SUCCESS, "保存成功");
        return message;
    }

}
