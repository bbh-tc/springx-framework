<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title><sitemesh:title default="百惠商务管理系统"/></title>
    <meta name="description" content="description"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <link type="image/x-icon" href="${ctx}/resources/images/favicon.ico" rel="shortcut icon">
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="${ctx}/resources/ace/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${ctx}/resources/ace/dist/css/font-awesome.min.css" />

    <!-- page specific plugin styles -->

    <!-- text fonts -->
    <link rel="stylesheet" href="${ctx}/resources/ace/dist/css/ace-fonts.min.css"/>
    <!-- ace styles -->
    <link rel="stylesheet" href="${ctx}/resources/ace/dist/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="${ctx}/resources/ace/dist/css/ace-part2.min.css" class="ace-main-stylesheet" />
    <![endif]-->

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="${ctx}/resources/ace/dist/css/ace-ie.min.css" />
    <![endif]-->

    <!--extend-->
    <link rel="stylesheet" href="${ctx}/resources/css/public.css" />

    <!-- ace settings handler -->
    <script src="${ctx}/resources/ace/dist/js/ace-extra.min.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="${ctx}/resources/ace/dist/js/html5shiv.min.js"></script>
    <script src="${ctx}/resources/ace/dist/js/respond.min.js"></script>
    <![endif]-->

    <!-- basic scripts -->
    <!--[if !IE]> -->
    <script src="${ctx}/resources/ace/dist/js/jquery.min.js"></script>
    <!-- <![endif]-->

    <!--[if IE]>
    <script src="${ctx}/resources/ace/dist/js/jquery1x.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
        if('ontouchstart' in document.documentElement) document.write("<script src='${ctx}/resources/ace/dist/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
    <script src="${ctx}/resources/ace/dist/js/bootstrap.min.js"></script>
    <script src="${ctx}/resources/js/public.js"></script>
    <script type="text/javascript" src="${ctx}/resources/ace/dist/js/bootbox.min.js"></script>
    <script type="text/javascript">var ctx="${ctx}";</script>
    <!--被装饰页面不要出现bootstrap的相关css-->
    <sitemesh:head/>
</head>

<body class="no-skin">
<%@ include file="/WEB-INF/layouts/layout0/navbar.jsp" %>
<!-- /section:basics/navbar.layout -->
<div class="main-container" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch (e) {
        }
    </script>
    <%@ include file="/WEB-INF/layouts/layout0/sidebar.jsp" %>
    <!-- /section:basics/sidebar -->
    <div class="main-content">
        <div class="main-content-inner">
            <%@ include file="/WEB-INF/layouts/layout0/breadcrumbs.jsp" %>
            <div class="page-content">
                <%@ include file="/WEB-INF/layouts/layout0/settings.jsp" %>
                <sitemesh:body/>
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/layouts/layout0/footer.jsp" %>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div>
<!-- /.main-container -->
</body>
<!-- ace scripts -->
<script src="${ctx}/resources/ace/dist/js/ace-elements.min.js"></script>
<script src="${ctx}/resources/ace/dist/js/ace.js"></script>
</html>