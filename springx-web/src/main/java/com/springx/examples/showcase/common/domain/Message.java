package com.springx.examples.showcase.common.domain;

import java.io.Serializable;

/**
 * ajax 向页面返回的信息
 */
public class Message implements Serializable {
    /**
     * 返回数据状态
     */
    private StatusType status;
    /***
     * 显示到前台的提示信息
     */
    private Object body;
    /**
     * 显示到前台的详细信息
     */
    private Object detail;
    /**
     * ajax返回携带数据
     */
    private Object data;
    public Object getDetail() {
        return detail;
    }

    public void setDetail(Object detail) {
        this.detail = detail;
    }

    public Message() {
    }

    public Message(StatusType status, Object body) {
        this.status = status;
        this.body = body;
    }
    public Message(StatusType status, Object body, Object detail) {
        this.status = status;
        this.body = body;
        this.detail = detail;
    }

    public StatusType getStatus() {
        return status;
    }

    public void setStatus(StatusType status) {
        this.status = status;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public static enum StatusType {
        SUCCESS,
        WARN,
        ERROR
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
