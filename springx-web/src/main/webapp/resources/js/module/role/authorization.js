if(typeof authorizationJs=='undefined'){
    authorizationJs={};
   /* authorizationJs.cancelHalf=function(treeNode) {
        if (treeNode.checkedEx) return;
        var zTree = $.fn.zTree.getZTreeObj("treeView");
        treeNode.halfCheck = false;
        zTree.updateNode(treeNode);
    }*/
/*    authorizationJs.onCheck=function (event, treeId, treeNode){
        treeNode.checkedEx = true;
        event.stopPropagation();
    }*/
  /*  authorizationJs.onAsyncSuccess=function (treeNode){
        authorizationJs.cancelHalf(treeNode);
    }*/
    authorizationJs.setting={
        async: {
            enable: true,
            url: ctx + "/resource/authorization",
            autoParam: ["id"]
        },check: {
            chkboxType: { "Y": "p", "N": "s" },
            enable: true
        },
        edit: {
            showRemoveBtn: false,
            enable: true,
            showRenameBtn: false
        }
    }
    authorizationJs.getChangeCheckedNodes=function (){
        var zTreeObj=authorizationJs.treeObj;
        return zTreeObj.getChangeCheckedNodes();
    }

    authorizationJs.init=function (){
        $.fn.zTree.init($("#treeView"),  authorizationJs.setting);
        authorizationJs.treeObj = $.fn.zTree.getZTreeObj("treeView");
    }
}

$(function (){
    authorizationJs.init();
})