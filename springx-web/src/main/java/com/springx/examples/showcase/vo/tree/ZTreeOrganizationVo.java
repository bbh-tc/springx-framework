package com.springx.examples.showcase.vo.tree;

/**
 * Created by roman_000 on 2015/7/12.
 */
public class ZTreeOrganizationVo extends ZTreeVo {

   private  String   description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
