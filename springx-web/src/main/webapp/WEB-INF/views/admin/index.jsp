<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>用户管理</title>
</head>
<body>
<form class="form-horizontal page-header" id="search-form" action="${ctx}/xx">
    <div class="form-group">
        <label class="col-xs-12 col-sm-1 control-label">用户名：</label>

        <div class="col-xs-12 col-sm-3">
            <input type="text" name="search_LIKE_username" class="input-large">
        </div>



        <label class="col-xs-12 col-sm-1 control-label">姓名：</label>

        <div class="col-xs-12 col-sm-3">
            <input type="text" name="search_LIKE_name" class="input-large">
        </div>

        <label class="col-xs-12 col-sm-1 control-label">创建时间：</label>
        <div class="col-xs-12 col-sm-3">
            <div class="input-group input-large">

                <input class="date-picker form-control" type="text" name="search_EQ_createDate"/>
                 <span class="input-group-addon">
                        <i class="fa fa-calendar bigger-110"></i>
                 </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-1 control-label">是否启用：</label>

        <div class="col-xs-12 col-sm-3">
            <select class="input-large" name="search_EQ_isEnabled">
                <option value=""></option>
                <c:forEach items="${enabledItems}" var="item">
                    <option value="${item.key}">${item.value}</option>
                </c:forEach>
            </select>
        </div>
        <label class="col-xs-12 col-sm-1 control-label">是否锁定：</label>

        <div class="col-xs-12 col-sm-3">
            <select name="search_EQ_isLocked" class="input-large">
                <option value=""></option>
                <c:forEach items="${lockedItems}" var="item">
                    <option value="${item.key}">${item.value}</option>
                </c:forEach>
            </select>
        </div>
        <label class="col-xs-12 col-sm-1 control-label"></label>

        <div class="col-xs-12 col-sm-3">
            <button class="btn btn-primary  btn-sm input-btn" type="button" onclick="doSearch()"><i
                    class="ace-icon fa fa-search bigger-120 white"></i>查找
            </button>
        </div>
    </div>
</form>
<div class="row">

    <div class="col-xs-12 custom_tools">
        <div id="indexAlert" class="hide" style="min-width:400px"></div>
        <button class="btn btn-primary  btn-sm" onclick="doAdd()"><i class="ace-icon fa fa-plus bigger-120 white"></i>增加
        </button>
        <button class="btn btn-danger btn-sm" onclick="doDelete()"><i
                class="ace-icon fa fa-trash-o bigger-120 white"></i>删除
        </button>
    </div>

</div>
<div class="row">
    <div class="col-xs-12">
        <table id="dynamic-table" class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th class="center sorting_disabled">
                    <label class="pos-rel">
                        <input type="checkbox" class="ace"/>
                        <span class="lbl"></span>
                    </label>
                </th>
                <th class="center">用户名</th>
                <th class="center">姓名</th>
                <th class="center">邮件</th>
                <th class="center">创建日期</th>
                <th class="center">是否启用</th>
                <th class="center">是否锁定</th>
                <th class="center">操作</th>
            </tr>
            </thead>
        </table>
    </div>
</div>

<!-- page specific plugin scripts -->
<c:import url="../common/validate.jsp"/>
<c:import url="../common/dataTable.jsp"/>
<c:import url="../common/datepicker.jsp"/>
<script src="${ctx}/resources/js/module/admin/admin.js"></script>
</body>