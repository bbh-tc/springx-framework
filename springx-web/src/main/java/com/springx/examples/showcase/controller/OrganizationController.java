package com.springx.examples.showcase.controller;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.springx.examples.showcase.common.domain.Message;
import com.springx.examples.showcase.common.domain.Order;
import com.springx.examples.showcase.entity.Organization;
import com.springx.examples.showcase.entity.Resource;
import com.springx.examples.showcase.service.OrganizationService;
import com.springx.examples.showcase.vo.tree.ZTreeOrganizationVo;
import com.springx.examples.showcase.vo.tree.ZTreeRootVo;
import com.springx.examples.showcase.vo.tree.ZTreeResourceVo;
import com.springx.examples.showcase.vo.tree.ZTreeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * Created by roman_000 on 2015/7/15.
 */
@Controller
@RequestMapping("/organization")
public class OrganizationController extends BaseController {
    @Autowired
    private OrganizationService organizationService;

    /**
     * 入口页面
     *
     * @return
     */
    @RequestMapping("/index")
    public String index(Model model) {

        return "organization/index";
    }

    @RequestMapping("/list")
    public
    @ResponseBody
    List list(Long id) {
        Map<String, Object> params = Maps.newHashMap();
        ZTreeRootVo rootVo;
        if (null != id) {
            params.put("parentId", id);
        }
        params.put(Order.PROPERTY_NAME, new Order("sort", Order.ASC));
        List<ZTreeOrganizationVo> list = organizationService.findChildren(params);
        if (null == id) {
            rootVo = ZTreeRootVo.initRoot("机构列表");
            rootVo.setChildren(list);
            List<ZTreeVo> rootTreeList = Lists.newArrayList();
            rootTreeList.add(rootVo);
            return rootTreeList;
        }
        return list;
    }


    /**
     * 保存机构
     *
     * @param organization
     * @return
     */
    @RequestMapping("/insert")
    public
    @ResponseBody
    Message insert(@Valid Organization organization) {
        organizationService.insert(organization);
        Message message = new Message(Message.StatusType.SUCCESS, "保存成功");
        message.setData(organization.getId());
        return message;
    }

    /**
     * 修改机构
     *
     * @param organization
     * @return
     */
    @RequestMapping("/update")
    public
    @ResponseBody
    Message update(@Valid Organization organization) {
        Assert.notNull(organization.getId());
        Organization editOrganization = organizationService.select(organization.getId());
        editOrganization.setName(organization.getName());
        editOrganization.setIsEnabled(organization.getIsEnabled());
        editOrganization.setDescription(organization.getDescription());
        organizationService.updateByPrimaryKeySelective(editOrganization);
        Message message = new Message(Message.StatusType.SUCCESS, "保存成功");
        return message;
    }


    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    public
    @ResponseBody
    Message delete(Long id) {
        Message message = null;
        Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        int count = organizationService.findChildrenCount(id);
        if (count > 0) {
            message = new Message(Message.StatusType.ERROR, "请先删除子节点");
        } else {
            organizationService.deleteByPrimaryKey(id);
            message = new Message(Message.StatusType.SUCCESS, "删除成功");
        }
        return message;
    }

    @RequestMapping("/sort")
    public
    @ResponseBody
    Message sort(Long id1, Long id2) {
        Assert.notNull(id1);
        Assert.notNull(id2);
        organizationService.sort(id1, id2);
        Message message = new Message(Message.StatusType.SUCCESS, "移动成功");
        return message;
    }


}
