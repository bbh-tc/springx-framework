package com.springx.examples.showcase.controller;

import com.springx.shiro.domain.Principal;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import com.springx.examples.showcase.common.spring.DateEditor;
import com.springx.examples.showcase.common.spring.HtmlCleanEditor;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by roman_000 on 2015/7/12.
 */
public class BaseController {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String PAGE_NUMBER = "_page";
    private static final String PAGE_SIZE = "_pageSize";
    protected static final String SUPER_ADMIN = "superAdmin";

    public PageRequest initPageRequest(HttpServletRequest request) throws ServletRequestBindingException {
        Integer pageNumber = ServletRequestUtils.getIntParameter(request, PAGE_NUMBER);
        Integer pageSize = ServletRequestUtils.getIntParameter(request, PAGE_SIZE);
        PageRequest pageRequest = new PageRequest(
                null == pageNumber ? 1 : pageNumber,
                null == pageSize ? 10 : pageSize
        );
        return pageRequest;
    }

    /**
     * 获取登录用户信息
     *
     * @return
     */
    public Principal getPrincipal() {
        Subject subject = SecurityUtils.getSubject();
        Principal principal = (Principal) subject.getPrincipal();
        return principal;
    }

    /**
     * 是否进行认证
     *
     * @return
     */
    public boolean isAuthenticated() {
        Subject subject = SecurityUtils.getSubject();
        return subject.isAuthenticated();
    }

    /**
     * 数据绑定
     *
     * @param binder WebDataBinder
     */
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new HtmlCleanEditor(true, true));
        binder.registerCustomEditor(Date.class, new DateEditor(true));
    }
}
