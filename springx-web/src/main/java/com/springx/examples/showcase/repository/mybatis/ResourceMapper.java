package com.springx.examples.showcase.repository.mybatis;
import com.springx.examples.showcase.entity.Resource;
import com.springx.examples.showcase.vo.tree.ZTreeNoneAjaxVo;
import com.springx.examples.showcase.vo.tree.ZTreeResourceVo;
import com.springx.shiro.domain.ShiroResource;
import tk.mybatis.mapper.common.Mapper;
import java.util.List;
import java.util.Map;
/**
 * Created by roman_000 on 2015/7/12.
 */
@MyBatisRepository
public interface ResourceMapper extends Mapper<Resource>{
    /**
     * 查询下级资源
     * @param params
     * @return
     */
    List<ZTreeResourceVo> findChildren(Map<String,Object> params);

    /**
     * 级联查询下级
     * @param params  id:id
     * @return
     */
    List<ZTreeNoneAjaxVo> selectTreeResourceChildren(Map<String,Object> params);


    /**
     * 查询用户menu
     * @param params
     * @return
     */
    List<ShiroResource> findMenuListByLoginName(Map<String,Object> params);

    /**
     * 查询所有menu
     * @param params
     * @return
     */
    List<ShiroResource> findAllMenuList(Map<String,Object> params);

}
