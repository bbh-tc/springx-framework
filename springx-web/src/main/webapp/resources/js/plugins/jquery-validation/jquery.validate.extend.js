/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ZH (Chinese, 中文 (Zhōngwén), 汉语, 漢語)
 */
jQuery.extend(jQuery.validator.messages, {
    required: "必填字段",
    remote: "请修正该字段",
    email: "请输入正确的邮件地址",
    url: "请输入合法的网址",
    date: "请输入合法的日期",
    dateISO: "请输入合法的日期 (ISO).",
    number: "请输入合法的数字",
    digits: "只能输入整数",
    creditcard: "请输入合法的信用卡号",
    equalTo: "请再次输入相同的值",
    accept: "请输入拥有合法后缀名的字符串",
    maxlength: jQuery.validator.format("请输入一个长度最多是 {0} 的字符串"),
    minlength: jQuery.validator.format("请输入一个长度最少是 {0} 的字符串"),
    rangelength: jQuery.validator.format("请输入一个长度介于 {0} 和 {1} 之间的字符串"),
    range: jQuery.validator.format("请输入一个介于 {0} 和 {1} 之间的值"),
    max: jQuery.validator.format("请输入一个最大为 {0} 的值"),
    min: jQuery.validator.format("请输入一个最小为 {0} 的值")
});
//默认值
jQuery.extend(jQuery.validator.defaults, {
    unhighlight:function (element){
        var $element=$(element);
        if( $element.is('input[type=checkbox]') ||  $element.is('input[type=radio]')){
            $element =  $element.parent();
        }
        $element.removeAttr("data-original-title");
        $element.tooltip("destroy");
    },
    errorPlacement: function (error, element) {
        /**
         if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
         else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
         else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
         else error.insertAfter(element.parent());
         **/
        if(element.is('input[type=checkbox]') || element.is('input[type=radio]')){
            element = element.parent();
        }
        var originalTitle=element.attr("data-original-title");
        if(originalTitle!=error.text()){
            element.attr("data-original-title",error.text());
            element.attr("data-placement","right");
            element.attr("data-trigger","manual")
            element.tooltip("show");
        }

    }
});

//指定位数的字符,数字或者下划线
jQuery.validator.addMethod("wRangeLength", function(value, element, params) {
    var reg= "/^\\w{"+params[0]+","+params[1]+"}$/";
    return this.optional(element) || eval(reg).test(value);
}, $.validator.format("请输入{0}-{1}位字符、数字或者下划线"));

//字母开头
jQuery.validator.addMethod("startLetters", function(value, element) {
    var reg=/^[a-z A-Z]+.*$/;
    return this.optional(element) || reg.test(value);
}, $.validator.format("请输入以字母开头的字符"));

//任意正则表达式
$.validator.addMethod("pattern", function(value, element, param) {
    if (this.optional(element)) {
        return true;
    }
    if (typeof param === "string") {
        param = new RegExp("^" + param + "$");
    }
    return param.test(value);
}, "格式错误");