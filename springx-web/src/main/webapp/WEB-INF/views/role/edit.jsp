<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<body>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" action="${ctx}/role/insert" method="post" id="editForm">
            <input type="hidden" name="id" value="${role.id}"/>

            <div class="form-group">
                <label class="col-md-2 control-label">角色名</label>

                <div class="col-md-8">
                    <input id="name" name="name" value="${role.name}" placeholder="请输入用户名"
                           class="form-control input-large"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">描述</label>

                <div class="col-md-8">
                    <textarea class="form-control" name="description" id="description"
                              class="form-control">${role.description}</textarea>
                </div>
            </div>

        </form>
    </div>
</div>
<div id="editAlert" class="hide" style="min-width:550px"></div>
</body>
</html>
