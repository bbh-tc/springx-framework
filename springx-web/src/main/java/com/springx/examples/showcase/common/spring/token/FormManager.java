package com.springx.examples.showcase.common.spring.token;
import javax.servlet.http.HttpServletRequest;
/**
 * 表单管理器，负责管理Session中的表单。
 *
 * @author tancheng
 */

public interface FormManager {
    /**
     * 生成一个新的表单
     */
    public Form createForm(HttpServletRequest request);
    /**
     * 判断表单是否存在。
     */
    public boolean hasForm(HttpServletRequest request, String token);
    /**
     * 访问参数中是否存在表单Token。
     */
    public boolean hasFormToken(HttpServletRequest request);
    /**
     * 销毁一个表单
     */
    public void removeToken(HttpServletRequest request, String token);
    
    /**
     * 销毁一个表单,并且新建立一个
     */
    public void removeAndCreateToken(HttpServletRequest request, String token);
    /**
     * 打印表单信息。
     */
    public String dumpForm(HttpServletRequest request, String token);
}
