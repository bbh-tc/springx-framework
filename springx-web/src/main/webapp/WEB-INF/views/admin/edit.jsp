<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html>
<body>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" action="${ctx}/admin/insert" method="post" id="editForm">
            <input type="hidden" name="id" value="${admin.id}"/>

            <div class="form-group">
                <label class="col-md-4 control-label">用户名</label>

                <div class="col-md-8">
                    <input id="username" name="username" value="${admin.username}" placeholder="请输入用户名"
                           class="form-control input-large"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">姓名</label>

                <div class="col-md-8">
                    <input id="name" name="name" value="${admin.name}" placeholder="请输入姓名"
                           class="form-control input-large"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">邮件</label>

                <div class="col-md-8">
                    <input id="email" name="email" value="${admin.email}" placeholder="请输入邮件地址"
                           class="form-control input-large"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">机构组织</label>

                <div class="col-md-8">
                    <input id="organizationId" name="organizationId" value="${admin.organizationId}"
                           placeholder="请选择机构组织"
                           class="form-control input-large"/>
                </div>
            </div>

            <c:if test="${empty admin.id}">
                <div class="form-group">
                    <label class="col-md-4 control-label">密码</label>

                    <div class="col-md-8">
                        <input id="password" name="password" placeholder="请输入密码"
                               class="form-control input-large"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">确认密码</label>

                    <div class="col-md-8">
                        <input id="repassword" name="repassword" type="text" placeholder="请输入确认密码"
                               class="form-control input-large"/>
                    </div>
                </div>
            </c:if>
            <div class="form-group">
                <label class="col-md-4 control-label">设置</label>

                <div class="col-md-8 checkbox-inline">
                    <label>
                        <input name="isEnabled" value="1" id="isEnabled" type="checkbox" class="ace"/>
                        <span class="lbl"> 启用</span>
                    </label>
                    <c:if test="${not empty admin.id}">
                        <label>
                            <input name="isLocked" value="1" id="isLocked" type="checkbox" class="ace"/>
                            <span class="lbl"> 锁定</span>
                        </label>
                    </c:if>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="editAlert" class="hide" style="min-width:550px"></div>
<script type="text/javascript">
    $(function () {
        $("#isEnabled").prop("checked","${admin.isEnabled}"=="true");
        $("#isLocked").prop("checked", "${admin.isLocked}"=="true");
    })

</script>
</body>
</html>
